class ApiResponseJson {
  bool status;
  List<String> message;
  dynamic data;


  ApiResponseJson(
      {this.status, this.message, this.data});

  ApiResponseJson.fromJson(Map<String, dynamic> json) {
    status = json['Status'];
    message = json['Message'].cast<String>();
    data = json['Data'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    data['Message'] = this.message;
    data['Data'] = this.data;
    return data;
  }
}