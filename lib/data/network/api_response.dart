class ApiResponse {
  String ResponseType;
  String ResponseCode;
 // dynamic object;
  String ResponseMessage;
  //dynamic results;


  ApiResponse(
      {this.ResponseType,
      this.ResponseCode,

      this.ResponseMessage,
      //this.results
  });

  ApiResponse.fromJson(Map<String, dynamic> json) {
    ResponseType = json['ResponseType'];
    ResponseCode = json['ResponseCode'];
    ResponseMessage = json['ResponseMessage'];
   // data = json['data'] = null? {"null"}: json['data'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ResponseType'] = this.ResponseType;
    data['ResponseCode'] = this.ResponseCode;
  //  data['token'] = this.token;
    data['ResponseMessage'] = this.ResponseMessage;
  //  data['results'] = this.results;
    return data;
  }
}

