import 'dart:async';
import 'dart:collection';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:testapp/data/network/api_response.dart';
import 'package:testapp/data/network/api.dart';
class RestAPIClient {
  Dio _dio;

  RestAPIClient(String  baseUrl) {
    Map<String, bool> extraParams = new HashMap();
    extraParams.putIfAbsent("withCredentials", () => true);

    BaseOptions options = new BaseOptions(
      /*  connectTimeout: 5000,
        receiveTimeout: 5000,*/
        baseUrl: baseUrl,
        extra: extraParams
    );
    _dio = Dio(options);
    _dio.interceptors.add(
        LogInterceptor(requestBody: true, responseBody: true));
  }

  Future<Response> get(apiName, {body, Map<String, dynamic> header, Options option}) async {
    Map<String, dynamic> headers = HashMap();

    if (header != null) {
      headers.addAll(header);
    }

    if (option != null) {
      option.headers = headers;
    } else {
      option = Options(method: "get");
      option.headers = headers;
    }

    try {
      Response response =
          await _dio.request(apiName, queryParameters: body, options: option);
      print("apiResponse=====>>>" + response.toString());

      if (response != null) {
       return response;

      }else {
        return null;
      }
      //return null;
    } catch (error) {
     print("error=> "+error.toString());
     return null;
    }
  }



}

//final RestAPIClient restClientNew = RestAPIClient();
final RestAPIClient restClientNew = RestAPIClient(Api.BASE_URL);