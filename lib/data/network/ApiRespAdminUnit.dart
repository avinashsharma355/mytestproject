class ApiResponseAdminUnit {
  dynamic AttributeList;
  String ResponseMessage;
 // dynamic object;
  String status;
  //dynamic results;


  ApiResponseAdminUnit(
      {this.AttributeList,
      this.ResponseMessage,

      this.status,
      //this.results
  });

  ApiResponseAdminUnit.fromJson(Map<String, dynamic> json) {
    AttributeList = json['AttributeList'];
    ResponseMessage = json['ResponseMessage'];
    status = json['status'];
   // data = json['data'] = null? {"null"}: json['data'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['AttributeList'] = this.AttributeList;
    data['ResponseMessage'] = this.ResponseMessage;
  //  data['token'] = this.token;
    data['status'] = this.status;
  //  data['results'] = this.results;
    return data;
  }
}

