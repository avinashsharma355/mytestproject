
import 'package:testapp/data/model/country_list.dart';
import 'package:testapp/data/network/RestAPIClient.dart';
import 'package:testapp/data/network/api.dart';
import 'package:dio/dio.dart';
abstract class CountryRepository {
  Future<List<Country_list>> getCountry();
}
class CountryRepositoryImpl implements CountryRepository{
  @override
  Future<List<Country_list>> getCountry() async {
    try {
      Response response = await restClientNew.get( Api.all_country);
      if (response != null) {
        List<Country_list> phasalStatusData = (response.data as List).map((itemWord) => Country_list.fromJson(itemWord)).toList();
        print("ressss=> "+phasalStatusData.length.toString());
        return phasalStatusData;
      }else{
        return null;
      }
    }catch (e) {
      return null;
    }
  }

}