import 'package:flutter/material.dart';
import 'package:testapp/data/model/country_list.dart';

class CountryListItem extends StatelessWidget {
  const CountryListItem({Key key, @required this.post}) : super(key: key);

  final Country_list post;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Material(
      child:Card(
        elevation: 8.0,
        margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
        child: Container(
          decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
          child:ListTile(
            contentPadding:
            EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
            leading: Container(
              padding: EdgeInsets.only(right: 12.0),
              decoration: new BoxDecoration(
                  border: new Border(
                      right: new BorderSide(width: 1.0, color: Colors.white24))),
              child: Icon(Icons.autorenew, color: Colors.white),
            ),
            title: Text(
              post.name,
              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
            subtitle: Column(
               crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  post.capital,
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.normal),
                ),
                Text(
                  post.population.toString(),
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.normal),
                ),

                Text(
                  post.currencies[0].name.toString(),
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.normal),
                )

              ],
            ),
            trailing:
            Icon(Icons.keyboard_arrow_right, color: Colors.white, size: 30.0),

          ),
        ),
      ),
    );
  }
}
