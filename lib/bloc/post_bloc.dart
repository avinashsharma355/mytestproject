import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:testapp/data/model/country_list.dart';
import 'package:testapp/data/repository/country_repository.dart';
part 'post_event.dart';
part 'post_state.dart';


class CountryBloc extends Bloc<PostEvent, CountryState> {
  CountryBloc() : super(const CountryState());


  @override
  Stream<Transition<PostEvent, CountryState>> transformEvents(
      Stream<PostEvent> events, TransitionFunction<PostEvent, CountryState> transitionFn) {
    return super.transformEvents(
      events,
      transitionFn,
    );

  }

  /* @override
  Stream<Transition<PostEvent, CountryState>> transformEvents(Stream<PostEvent> events, TransitionFunction<PostEvent, CountryState> transitionFn,) {
    return super.transformEvents(
      events.throttleTime(const Duration(milliseconds: 500)),
      transitionFn,
    );
  }*/
  Future<List<Country_list>> _fetchPosts([int startIndex = 0]) async {
return  new CountryRepositoryImpl().getCountry();

  }

  @override
  Stream<CountryState> mapEventToState(PostEvent event)  async* {
    // TODO: implement mapEventToState
    if (event is PostFetched) {
      yield await _mapPostFetchedToState(state);
    }
  }


  Future<CountryState> _mapPostFetchedToState(CountryState state) async {
    if (state.hasReachedMax) return state;
    try {
      if (state.status == CountryStatus.initial) {
        final posts = await _fetchPosts();
        if(posts==null){
          return state.copyWith(
            status: CountryStatus.failure,
            posts: posts,
            hasReachedMax: false,
          );
        }else
          return state.copyWith(
          status: CountryStatus.success,
          posts: posts,
          hasReachedMax: false,
        );
      }
      final posts = await _fetchPosts(state.posts.length);
      return posts.isEmpty
          ? state.copyWith(hasReachedMax: true)
          : state.copyWith(
        status: CountryStatus.success,
        posts: List.of(state.posts)..addAll(posts),
        hasReachedMax: false,
      );
    } on Exception {
      return state.copyWith(status: CountryStatus.failure);
    }
  }
}
