part of 'post_bloc.dart';

enum CountryStatus { initial, success, failure }

class CountryState extends Equatable {
  const CountryState({
    this.status = CountryStatus.initial,
    this.posts = const <Country_list>[],
    this.hasReachedMax = false,
  });

  final CountryStatus status;
  final List<Country_list> posts;
  final bool hasReachedMax;

  CountryState copyWith({
    CountryStatus status,
    List<Country_list> posts,
    bool hasReachedMax,
  }) {
    return CountryState(
      status: status ?? this.status,
      posts: posts ?? this.posts,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
    );
  }

  @override
  String toString() {
    return '''PostState { status: $status, hasReachedMax: $hasReachedMax, posts: ${posts.length} }''';
  }

  @override
  List<Object> get props => [status, posts, hasReachedMax];
}
