import 'package:flutter/cupertino.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testapp/bloc/post_bloc.dart';
import 'package:testapp/data/model/country_list.dart';
import 'package:testapp/widgets/bottom_loader.dart';
import 'package:testapp/widgets/post_list_item.dart';

class CountryList extends StatefulWidget {
  @override
  _CountryListState createState() => _CountryListState();
}

class _CountryListState extends State<CountryList> {
  final _scrollController = ScrollController();
  CountryBloc _postBloc;
  TextEditingController editingController = TextEditingController();
  List<Country_list> clist;

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
    _postBloc = context.read<CountryBloc>();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CountryBloc, CountryState>(
      builder: (context, state) {
        switch (state.status) {
          case CountryStatus.failure:
            return const Center(child: Text('failed to fetch posts'));
          case CountryStatus.success:
            if (state.posts.isEmpty) {
              return const Center(child: Text('No Record Found'));
            }
            clist = state.posts;
            return clist == null
                ? BottomLoader()
                : Container(
                    child: Column(
                      children: <Widget>[
                        Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              children: [
                                Expanded(
                                  child: TextField(
                                    onChanged: (value) {
                                      setState(() {});
                                    },
                                    controller: editingController,
                                    decoration: InputDecoration(
                                        labelText: "Search",
                                        hintText:
                                            "search by Name,Capital,Population.",
                                        prefixIcon: Icon(Icons.search),
                                        border: OutlineInputBorder(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(25.0)))),
                                  ),
                                  flex: 4,
                                ),
                                Expanded(
                                  child: InkWell(
                                    child: Icon(Icons.filter_alt_outlined),
                                    onTap: () {
                                      showbottomSheet();
                                    },
                                  ),
                                  flex: 1,
                                )
                              ],
                            )),
                        Expanded(
                          child: ListView.builder(
                              shrinkWrap: true,
                              itemCount: clist.length,
                              itemBuilder: (context, index) {
                                if (editingController.text.isEmpty) {
                                  return CountryListItem(post: clist[index]);
                                } else if (clist[index]
                                        .name
                                        .toLowerCase()
                                        .contains(editingController.text) ||
                                    clist[index]
                                        .capital
                                        .toLowerCase()
                                        .contains(editingController.text) ||
                                    clist[index].population.toString() ==
                                        editingController.text) {
                                  return CountryListItem(post: clist[index]);
                                } else {
                                  return Container();
                                }
                              }),
                        ),
                      ],
                    ),
                  );
          default:
            return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }
void showbottomSheet(){

  showModalBottomSheet(
      context: context,
      builder: (context) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              leading:
              new Icon(Icons.sort_by_alpha),
              title:
              new Text('Sort by Name '),
              onTap: () {
                Navigator.pop(context);

                clist.sort((a, b) => a.name
                    .compareTo(b.name));
                setState(() {});
              },
            ),
            ListTile(
              leading: new Icon(
                  Icons.sort_by_alpha),
              title: new Text(
                  'Sort by Capital'),
              onTap: () {
                Navigator.pop(context);

                clist.sort((a, b) => a
                    .capital
                    .compareTo(b.capital));
                setState(() {});
              },
            ),
            ListTile(
              leading:
              new Icon(CupertinoIcons.money_dollar),
              title: new Text(
                  'Sort by Currency'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading:
              new Icon(Icons.sort_sharp),
              title: new Text(
                  'Sort by Population'),
              onTap: () {
                Navigator.pop(context);
                clist.sort((a, b) =>
                    b.population.compareTo(
                        a.population));  setState(() {});
              },
            ),
          ],
        );
      });
}
  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    if (_isBottom) _postBloc.add(PostFetched());
  }

  bool get _isBottom {
    if (!_scrollController.hasClients) return false;
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.offset;
    return currentScroll >= (maxScroll * 0.9);
  }
}
